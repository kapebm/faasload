#! /usr/bin/env python3
"""Create OpenWhisk assets to run FaaSLoad in dataset generation mode.

 1. load functions to FaaSLoad's database
 2. create OpenWhisk users for the generated workload injection (one per function to run)
 3. create actions in OpenWhisk under each user

This is a standalone runnable script (running the method `main`).

SQL queries:

 * `CREATE_TABLE`: create the table to import the functions
 * `INSERT_FUNCTION`: import one function into the database

Constants:

 * `AUTHKEY_DIR`: output directory where authentication tokens of generated users are written
 * `LIMIT_NAMES`: a mapping between the limit names in the Manifest, and the names of the limit fields in an Action

Methods:

 * `read_functions`: read functions (as instances of Action) from the Manifest
 * `load_to_database`: load the Action instances into the database
 * `load_functions_faasload`: load functions from the Manifest to FaaSLoad's database
 * `create_users_wsk`: create OpenWhisk users to run the functions
 * `load_actions_wsk`: load functions to OpenWhisk
"""

import logging
import os
import sys
from base64 import b64encode

import mysql.connector as dbc
import yaml
from pywhisk.action import create as create_action
from pywhisk.admin.user import create as create_user
from pywhisk.client import AdministrationConfiguration, OpenWhiskException, read_api_cfg as read_wsk_cfg
from pywhisk.models import Action, ActionExec, ActionLimits, Package
from pywhisk.package import create as create_package

from faasload.loader import DEFAULTS as FAASLOAD_DEFAULTS, OpenWhiskConfiguration
from faasload.utils import try_read_configuration

AUTHKEY_DIR = '../generator_users'

CREATE_TABLE = """
CREATE OR REPLACE TABLE `functions` (
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(256),
    `runtime` VARCHAR(64),
    `image` VARCHAR(256),
    `parameters` VARCHAR(2048),
    `input_kind` ENUM('audio', 'image', 'video', 'text'),
    INDEX (`name`)
)
"""

INSERT_FUNCTION = """
INSERT INTO `functions` (`name`, `runtime`, `image`, `parameters`, `input_kind`)
VALUES (%(name)s, %(runtime)s, %(image)s, %(parameters)s, %(input_kind)s)
"""


def read_functions(manifest_path):
    """Read functions from the Manifest.

    :param manifest_path: path to the Manifest
    :type manifest_path: str

    :return: dictionary mapping package names to dicts, mapping functions names to Action
    """
    with open(manifest_path, 'r') as manifest_file:
        manifest = yaml.full_load(manifest_file)

    return {
        pkg_name: {
            action_name: _compose_action(action_dict, os.path.dirname(manifest_path))
            for action_name, action_dict in pkg_dict['actions'].items()
        }
        for pkg_name, pkg_dict in manifest['packages'].items()
    }


LIMIT_NAMES = {
    'memorySize': 'memory',
    'timeout': 'timeout',
    'logSize': 'logs',
    'concurrentActivations': 'concurrency',
}


def _compose_action(d, function_basepath):
    """Create an Action from the dictionary read from the Manifest.

    :param d: dictionary representing an action as loaded from the Manifest file
    :type d: dict
    :param function_basepath: base path of functions' codes
    :type function_basepath: str

    :return: the Action
    """

    with open(os.path.join(function_basepath, d['function']), 'rb') as code_file:
        try:
            ex = ActionExec(kind=d['runtime'], binary=True, code=b64encode(code_file.read()).decode())
        except KeyError:
            ex = ActionExec(kind='blackbox', binary=True, image=d['docker'], code=b64encode(code_file.read()).decode())

    limits = ActionLimits.from_dict(
        {LIMIT_NAMES[limit_name]: limit_value for limit_name, limit_value in d['limits'].items()})

    return Action(exec=ex,
                  limits=limits,
                  annotations=[{'key': anno_name, 'value': anno_value} for anno_name, anno_value in
                               d['annotations'].items()])


def load_to_database(functions, db_cfg):
    """Load the functions to the database.

    :param functions: functions to load to database (mapping {pkg: {name: Action}})
    :type functions: dict[str, dict[str, Action]]
    :param db_cfg: configuration to connect to the database
    :type db_cfg: DatabaseConfiguration

    `functions` can be read from the Manifest using `read_functions()` and passed directly to this function.
    """

    functions_cnx = dbc.connect(**db_cfg._asdict())
    functions_cur = functions_cnx.cursor()
    functions_cur.execute(CREATE_TABLE)

    for package_name, package in functions.items():
        for function_name, function in package.items():
            parameters = [a['value'] for a in function.annotations if a['key'] == 'parameters'][0]
            input_kind = [a['value'] for a in function.annotations if a['key'] == 'input_kind'][0]

            function_dict = {
                'name': package_name + '/' + function_name,
                'runtime': function.exec.kind,
                'image': function.exec.image if function.exec.kind == 'blackbox' else None,
                'parameters': yaml.dump(parameters),
                'input_kind': input_kind,
            }

            functions_cur.execute(INSERT_FUNCTION, function_dict)

            logging.info('\tloaded function %s: %s', function_name, function_dict)

        logging.info('\tloaded package %s', package_name)

    functions_cnx.commit()

    functions_cur.close()
    functions_cnx.close()


def load_functions_faasload(manifest_path, db_cfg):
    """Load the functions to FaaSLoad's database.

    :param manifest_path: the path to the Manifest
    :type manifest_path: str
    :param db_cfg: the configuration to connect to the database
    :type db_cfg: DatabaseConfiguration

    :returns: the list of functions read from the manifest (mapping {pkg: {name: Action}})
    """
    funcs = read_functions(manifest_path)
    nb_funcs = sum(len(package) for package in funcs.values())

    logging.info('\tread %d functions from "%s"', nb_funcs, manifest_path)

    load_to_database(funcs, db_cfg)

    logging.info('\tloaded %d functions to table `functions` of database `%s` with user \'%s\'',
                 nb_funcs, db_cfg.database, db_cfg.user)

    return funcs


def create_users_wsk(functions, wsk_admin_cfg):
    """Create user in OpenWhisk to run the functions.

    :param functions: the functions to create users for (mapping {pkg: {name: Action}})
    :type functions: dict[str, dict[str, Action]]
    :param wsk_admin_cfg: the configuration to use administrative facilities of OpenWhisk
    :type wsk_admin_cfg: AdministrationConfiguration

    :returns: the mapping of function names to usernames (mapping {pkg: {name: (username, auth)}})
    """
    func2user = {}

    os.makedirs(AUTHKEY_DIR, exist_ok=True)

    for package_name, package in functions.items():
        func2user[package_name] = {}
        for func_name in package:
            user_name = 'user-' + func_name

            user_auth = create_user(user_name, wsk_admin_cfg)
            func2user[package_name][func_name] = (user_name, user_auth)
            with open(os.path.join(AUTHKEY_DIR, user_name), 'x') as auth_file:
                auth_file.write(user_auth[0] + ':' + user_auth[1] + '\n')

            logging.info('\tcreated user %s for function %s/%s', user_name, package_name, func_name)

    return func2user


def load_actions_wsk(funcs, func2user, wsk_cfg):
    """Load actions to OpenWhisk under their matching users.

    :param funcs: the functions to create users for (mapping {pkg: {name: Action}})
    :type funcs: dict[str, dict[str, Action]]
    :param func2user: the mapping of function names to usernames (mapping {pkg: {name: (username, auth)}})
    :type func2user: dict[str, dict[str, tuple[str, tuple[str, str]]]]
    :param wsk_cfg: the configuration to use OpenWhisk
    :type wsk_cfg: OpenWhiskConfiguration
    """
    for package_name, package in funcs.items():
        for func_name, func in package.items():
            user_name, _ = func2user[package_name][func_name]

            create_package(Package(), package_name, user_name, wsk_cfg)

            create_action(func, package_name + '/' + func_name, user_name, wsk_cfg)

            logging.info('\tloaded action %s/%s to OpenWhisk under user %s', package_name, func_name, user_name)


def main():
    """Main method of the script."""
    import urllib3

    wsk_assets = True
    manifest_path = None
    for arg in sys.argv[1:]:
        if arg == '--db-only':
            wsk_assets = False
        else:
            if not manifest_path:
                manifest_path = arg
            else:
                # another unidentified argument: this is an error
                manifest_path = None
                break
    if not manifest_path:
        logging.error(f'Usage: {sys.argv[0]} [--db-only] MANIFEST')
        logging.error('Create OpenWhisk assets to run FaaSLoad generator.')
        logging.error(
            'MANIFEST is the path to the manifest file describing the OpenWhisk actions to use with the generator.')
        logging.error(
            '--db-only tells the script to only create the table `functions` for FaaSLoad, i.e. to not create assets '
            'for OpenWhisk.')
        logging.error('See "docs/generator.md", section "Actions for dataset generation".')

        sys.exit(2)

    conf = try_read_configuration(os.path.expanduser('~/.config/faasload/loader.yml'), FAASLOAD_DEFAULTS)

    db_cfg = conf['database']

    # disable certificate checking because the self-signed certificate of local Ansible deployments is broken
    wsk_cfg = OpenWhiskConfiguration(kafkahost=conf['openwhisk'].kafkahost,
                                     **read_wsk_cfg(cert=not conf['openwhisk'].disablecert)._asdict())
    # and disable associated warning
    urllib3.disable_warnings()

    wsk_admin_cfg = AdministrationConfiguration(os.path.expanduser(os.path.expandvars(conf['openwhisk'].home)))

    logging.info('Loading functions to FaaSLoad\'s database...')
    funcs = load_functions_faasload(manifest_path, db_cfg)
    logging.info('Done')

    if not wsk_assets:
        return

    logging.info('Creating OpenWhisk users...')
    func2user = create_users_wsk(funcs, wsk_admin_cfg)
    logging.info('Done')

    # update our WSK config with authentication of new users
    wsk_cfg.auth.update(user_auth for package in func2user.values() for user_auth in package.values())

    logging.info('Loading actions to OpenWhisk...')
    try:
        load_actions_wsk(funcs, func2user, wsk_cfg)
    except OpenWhiskException:
        logging.exception('failed loading actions to OpenWhisk')
        sys.exit(1)
    else:
        logging.info('Done')

    logging.info('And all done!')


if __name__ == '__main__':
    main()
