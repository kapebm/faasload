#! /usr/bin/env python3
import logging
import os
import statistics
import sys
from datetime import datetime
from glob import glob

import pywhisk.activation as wsk_act
import urllib3
from pywhisk.admin.docker import get_docker_id as whisk_get_docker_id
from pywhisk.client import AdministrationConfiguration, OpenWhiskException, read_api_cfg as read_wsk_cfg

from faasload.docker_monitor.monitor import Measurement
from faasload.loader import DEFAULTS as FAASLOAD_DEFAULTS, OpenWhiskConfiguration
from faasload.loader.database import FaaSLoadDatabase, FaaSLoadDatabaseException
from faasload.loader.injection import get_resource_usage
from faasload.utils import try_read_configuration

NAMESPACE = '_'


def _read_auth_keys(auth_dir):
    auth = {}
    for filepath in glob(os.path.join(auth_dir, '*')):
        with open(filepath, 'r') as auth_file:
            auth[os.path.basename(filepath)] = tuple(auth_file.read().rstrip().split(':', maxsplit=1))

    return auth


def main():
    write_db = False
    act_id = None
    for arg in sys.argv[1:]:
        if arg == '--write-db':
            write_db = True
        else:
            if not act_id:
                act_id = arg
            else:
                # another unidentified argument: this is an error
                act_id = None
                break
    if not act_id:
        logging.error(f'Usage: {sys.argv[0]} ACTIVATIONID')
        logging.error('Fetch resource usage from the Docker monitor.')
        logging.error('ACTIVATIONID is the ID of the OpenWhisk activation of interest.')

        sys.exit(2)

    conf = try_read_configuration(os.path.expanduser('~/.config/faasload/loader.yml'), FAASLOAD_DEFAULTS)

    docker_mon_cfg = conf['dockermonitor']._replace(
        measurementsock=os.path.expanduser(os.path.expandvars(conf['dockermonitor'].measurementsock)))

    db_cfg = conf['database']

    # disable certificate checking because the self-signed certificate of local Ansible deployments is broken
    wsk_cfg = OpenWhiskConfiguration(kafkahost=conf['openwhisk'].kafkahost,
                                     **read_wsk_cfg(cert=not conf['openwhisk'].disablecert)._asdict())
    # and disable associated warning
    urllib3.disable_warnings()
    # read additional authorization keys if specified
    if conf['openwhisk'].authkeys:
        wsk_cfg.auth.update(_read_auth_keys(os.path.expanduser(os.path.expandvars(conf['openwhisk'].authkeys))))

    wsk_admin_cfg = AdministrationConfiguration(os.path.expanduser(os.path.expandvars(conf['openwhisk'].home)))

    try:
        act = wsk_act.get(act_id, NAMESPACE, wsk_cfg)
    except OpenWhiskException as err:
        logging.critical('failed fetching activation data for activation %s of user %s', act_id,
                         NAMESPACE)
        logging.critical('reason: %s', err)
        return

    try:
        cont_id = whisk_get_docker_id(act_id, wsk_admin_cfg)
    except ValueError:
        logging.critical('failed getting container ID of activation %s of user %s',
                         act.activationId, NAMESPACE)
        return

    try:
        meas = get_resource_usage(docker_mon_cfg.measurementsock, cont_id, act.start, act.end)
    except (ValueError, OSError) as err:
        logging.critical('failed fetching resource usage measurements of container %s: %s', cont_id, err)
        sys.exit(1)

    if write_db:
        db = FaaSLoadDatabase(db_cfg)

        func = db.select_function(act.name)

        rec_id = db.insert_partial_run({
            'function_id': func.id,
            'namespace': NAMESPACE,
            'activation_id': act_id,
        })
        # note: cannot get parameters from activation record
        db.insert_parameters(rec_id, {})

        db.update_activation(act_id, {
            'failed': not act.response.success,
            'start': act.start,
            'end': act.end,
            'wait_time': next((a for a in act.annotations if a['key'] == 'waitTime'), {'value': None})['value'],
            'init_time': next((a for a in act.annotations if a['key'] == 'initTime'), {'value': None})['value'],
            'output_size': None,
            'extract_time': None,
            'transform_time': None,
            'load_time': None,
        })

        try:
            db.insert_resources(act.activationId, meas)
        except FaaSLoadDatabaseException:
            logging.critical(
                'failed storing resource usage measurements of activation %s of user %s into the database',
                act.activationId, NAMESPACE, exc_info=True)

    meas = [Measurement(**rec) for rec in meas]

    # And then, do something with the fetched measurements. There are probably a lot of records, so printing them all is
    # a bad idea, and it is better to display a summary.
    memory_recs = [rec.memory for rec in meas]
    cpu_recs = [rec.cpu for rec in meas]
    print(
        f'Fetched measurements for Docker container {cont_id[:12]} between {datetime.fromtimestamp(act.start / 1000)} '
        f'and {datetime.fromtimestamp(act.end / 1000)}')
    print(
        f'{len(meas)} records, first at {datetime.fromtimestamp(meas[0].date)}, last at '
        f'{datetime.fromtimestamp(meas[-1].date)}')
    print(
        f'Maximum memory usage: {max(memory_recs) / (1024 * 1024):.1f}MB, average: '
        f'{statistics.fmean(memory_recs) / (1024 * 1024):.1f}MB')
    print(f'Maximum CPU usage: {max(cpu_recs)}mCPU, average: {statistics.fmean(cpu_recs):.3f}mCPU')


if __name__ == '__main__':
    main()
