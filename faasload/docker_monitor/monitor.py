"""
Module for the monitor threads.

Classes:

 * 'DockerMonitorThread': the class of the monitor threads;
 * 'Measurement': namedtuple to store one measurement of resource (memory and CPU) usage of a container at a given date.

Measurements are taken at most every 'measurement_resolution' seconds, this is not an exact clock. There is a check in
the monitoring infinite loop that warns for missed measurements, saying 'missed measurements for container [...]'.
"""

import logging
import os
import re
import signal
import subprocess
import time
import fcntl

from collections import namedtuple
from threading import Event, Thread
from typing import List

from pycgroup import CGroup
from pycgroup.controllers import cpuacct as cgroup_cpu, memory as cgroup_memory

Measurement = namedtuple('Measurement', ['date', 'memory', 'cpu', 'perf'])


class DockerMonitorThread(Thread):
    """
    Monitor thread.

    Monitor a Docker container used by OpenWhisk to run actions. The monitoring consists in storing periodic
    measurements of the memory and CPU usage of the container. Measurements are stored in a mutable data structure
    passed at creation time.

    ## Measurements

    Each measurement (a 'Measurement' namedtuple) has its date stored as an absolute timestamp (in seconds,
    rounded to milliseconds); timestamps are not offset by the timestamp of the first measurement.

    Measurements may have None as memory or CPU value. This indicates failure reading the resource usage at this time.

    For memory, the measurements store the memory usage at the time (in bytes). It is the sum of the resident set size
    (RSS) and of the total mapped file size.

    For CPU, the measurements store the cumulative usage at the time (in seconds of CPU time, rounded to milliseconds)
    since the beginning of execution **of the container**. The monitor thread does not process the measurement, so you
    must process the values afterwards with at least the first of the following steps:

     * offset the starting CPU cumulative usage;
     * compute 'instantaneous' CPU usages as percentage of CPU time: for two measurements c1 and c2 at times t1 and t2
        respectively, the instantaneous usage at (t1+t2)/2 is (c2-c1)/(t2-t1)

    ## Monitoring

    The monitoring is done by reading usage stats from the container's cgroup ('control groups', a feature of the Linux
    kernel). The thread is an infinite loop that reads the usage stats, stores them, then sleeps for some duration.

    The thread can be paused, and will block until resumed. It can also be terminated, which makes it return from its
    infinite loop.
    """

    def __init__(self, container_id, container_measurements, measurement_resolution, perf_counters):
        """Create a new monitor thread.

        :param container_id: the ID of the container to monitor (the name of its cgroup under Docker's root cgroup)
        :type container_id: str
        :param container_measurements: the mutable data structure used to store measurements (see below)
        :type container_measurements: list

        'container_measurements' must be mutable and support the 'append' method, so it should be list-like. In
        practice, it might grow indefinitely if the monitored container is never destroyed, and the thread **will not
        shrink it**. Thus, it is a good idea to use a self-maintaining data structure such as a 'deque' with a max
        length set.
        """
        name = 'monitor-' + container_id[:12]

        super().__init__(name=name, daemon=True)

        self.container_id = container_id

        # Take references to the cgroups, only for monitoring, no control (they are Docker's)
        self.mem_cgrp = CGroup(cgroup_memory, 'docker/' + self.container_id, reuse=True)
        self.cpu_cgrp = CGroup(cgroup_cpu, 'docker/' + self.container_id, reuse=True)

        self.measurements = container_measurements
        self.perf_counters = perf_counters

        self.resolution = measurement_resolution

        self.missed = 0

        self.logger = logging.getLogger('monitor')

        self._monitor = Event()
        self._monitor.set()
        self._terminate = Event()
        self.perf_process = None

        # Used to suppress missed measurement check after resuming
        self.just_resumed = True

    def perf_container_periodically(self, idc: str, period: int, metrics: List[str]) -> subprocess.Popen:
        """
        Measure the metrics counted periodically by the following command
        perf stat -I <period> -e <metrics> --cgroup=docker/<idc>
        :param idc: string representing container's id
        :param period: time in msec between 2 measurements
        :param metrics: list of metrics name e.g ["cpu-clock", "cache-misses", ...]
        :return: the process of measurement to parse output
        """
        seq = [
            "perf", "stat",
            "-I", str(period),
            "-e", ",".join(metrics),
            "--cgroup=docker/{}".format(idc),
        ]
        self.logger.debug("command : " + " ".join(seq))
        return subprocess.Popen(seq, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    def parse_stat(self, perf: subprocess.Popen, metrics: List[str]) -> (dict, float):
        """
        Parse stdout of the command perf stat -e <metrics>
        :param perf: perf stat command (POpen)
        :param metrics: list of metrics name e.g ["cpu-clock", "cache-misses", ...]
        :return: dictionary of metrics and measurements and timestamp of the measure
        """
        res = dict.fromkeys(metrics, None)
        i = 0
        # make stdout a non blocking file
        fd = perf.stdout.fileno()
        # not a good way of doing this
        # better way https://stackoverflow.com/a/4896288
        fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        date = None

        self.logger.debug("starting loop to parse perf output")
        for line in perf.stdout:
            s = str(line.rstrip())
            if metrics[i] in s:
                s = s.split(metrics[i])[0]
                self.logger.debug(s)
                if '<not counted>' in s:
                    meas = None
                    date = float(re.search(r'\d+(\.\d+)?', s).group())
                else:
                    s = s.replace(',', '')
                    # use regex to find float or int like 25.04 or 255 in str
                    date, meas = map(float, re.findall(r'(\d+\.\d+|\d+)', s))

                res[metrics[i]] = meas
                i += 1
                if i >= len(metrics):
                    break
        self.logger.debug("parsed perf metrics : {}".format(res))
        self.logger.debug("parsed date : {}".format(date))
        return res, date

    def run(self):
        # both variables used to log only once in a row
        failing_memory = False
        failing_cpu = False
        self.perf_process = self.perf_container_periodically(
            self.container_id,
            int(self.resolution*1000),
            self.perf_counters)

        perf_stats = dict.fromkeys(self.perf_counters)
        start = round(time.time(), 3)
        self.logger.debug("starting main loop in run function")
        while True:
            self._monitor.wait()
            if self._terminate.is_set():
                self.logger.info('terminate')
                break

            date = round(time.time(), 3)

            # check that we did not miss measurements
            if self.just_resumed:
                self.just_resumed = False
            else:
                previous = self.measurements[-1].date
                skipped = date - previous
                if skipped > 2 * self.resolution:
                    missed = skipped // self.resolution
                    self.missed += missed
                    self.logger.warning(
                        'missed %d measurements for container %s between %f and %f (total missed: %d/%d, %.1f%%)',
                        missed,
                        self.container_id,
                        previous, date,
                        self.missed,
                        len(self.measurements) + self.missed,
                        100 * self.missed / (len(self.measurements) + self.missed))

            try:
                memory = self.mem_cgrp.stat['total_rss'] + self.mem_cgrp.stat['total_mapped_file']
                failing_memory = False
            except OSError as err:
                if not failing_memory:
                    self.logger.error('failed reading memory usage of container %s: %s', self.container_id, err)
                    failing_memory = True
                memory = None
            try:
                cpu = round(self.cpu_cgrp.usage / 1000000000, 3)
                failing_cpu = False
            except OSError as err:
                if not failing_cpu:
                    self.logger.error('failed reading CPU usage of container %s: %s', self.container_id, err)
                    failing_cpu = True
                cpu = None

            try:
                if len(self.perf_counters) != 0:
                    perf_stats, perf_ts = self.parse_stat(self.perf_process, self.perf_counters)
                    while perf_ts and ((perf_ts - (date - start)) > self.resolution):
                        self.logger.warning("perf measure is out-of-date, need a new one")
                        perf_stats, perf_ts = self.parse_stat(self.perf_process, self.perf_counters)

            except OSError as e:
                self.logger.error('failed measurement with perf command')
                perf_stats = None

            self.measurements.append(Measurement(date, memory, cpu, perf_stats))
            self.logger.debug(self.measurements)
            time.sleep(self.resolution)

        self.perf_process.terminate()

    def pause(self):
        """Pause the monitoring thread: no more measurements are stored until resumed."""
        self.perf_process.send_signal(signal.SIGTSTP)
        self._monitor.clear()

    def resume(self):
        """Resume the paused monitoring thread."""
        self.just_resumed = True
        self.perf_process.send_signal(signal.SIGCONT)
        self._monitor.set()

    def terminate(self):
        """Terminate the monitoring thread."""
        # terminate the perf subprocess
        self.perf_process.terminate()
        self.perf_process.communicate()
        # Resume the monitor by setting _monitor to unblock it
        self._monitor.set()
        self._terminate.set()

