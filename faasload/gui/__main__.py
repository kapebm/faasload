import PySimpleGUI as sg

import logging
from gui import draw_figure, delete_fig_agg, clean_data_for_display
from const import db, fig_down, fig_up, functions_to_display, perf_counters, res_dict, sql_res


def main():
    """TW some duplicate code here can be simplified"""
    logger = logging.getLogger('main')
    logger.debug("sql_res = {}".format(sql_res))
    logger.debug("Dropdown: {}".format(functions_to_display))
    logger.debug("res_dict = {}".format(res_dict))

    fig_canvas_agg_up = None
    fig_canvas_agg_down = None

    layout = [[sg.Text('Select data to display', size=(25, 2), justification='left'),
               sg.Text('Graph(s)', size=(25, 2), justification='right')],
              [sg.InputCombo(functions_to_display, key='UP ACTION'), sg.Canvas(key='UP CANVAS')],
              [sg.InputCombo(functions_to_display, key='DOWN ACTION'), sg.Canvas(key='DOWN CANVAS')],
              [sg.InputCombo(perf_counters, key='COUNTER')],  # les noms doivent être ceux utilisés dans le monitor
              [sg.Text('', size=(25, 1), key='ERROR')],
              [sg.Button('Show'), sg.Button('Exit')]]

    window = sg.Window('FAASLoad graphs', layout, font='Helvetica 14', no_titlebar=True, alpha_channel=.8,
                       grab_anywhere=True)
    sg.theme('DarkGreen3')
    while True:  # Event Loop
        event, values = window.read()

        if event == sg.WIN_CLOSED or event == 'Exit':
            break

        if event == 'Show':
            # Cleaning the window
            if fig_canvas_agg_up is not None:
                delete_fig_agg(fig_canvas_agg_up)
            if fig_canvas_agg_down is not None:
                delete_fig_agg(fig_canvas_agg_down)

            # Fetching data
            counter = values['COUNTER']
            logger.debug("Fetching ids")
            clef_up = values['UP ACTION']
            clef_down = values['DOWN ACTION']
            logger.debug("clef_up = {}".format(clef_up))
            if counter != "":  # A counter MUST be selected
                window['ERROR'].update("")
                if clef_up != "":
                    id_up = res_dict[clef_up]
                    logger.debug(f"id_up={id_up}")
                    data_up = db.select_resources(id_up, counter)
                    # Clean data for display
                    data_up = clean_data_for_display(data_up)
                    logger.debug(f"data_up={data_up}")

                    # display code
                    ax_up = fig_up.add_subplot(111)
                    ax_up.set_xlabel("time (ms)")
                    ax_up.set_ylabel(counter)
                    ax_up.plot(*zip(*data_up))
                    fig_canvas_agg_up = draw_figure(window['UP CANVAS'].TKCanvas, fig_up)
                if clef_down != "":
                    id_down = res_dict[clef_down]
                    logger.debug(f"id_down={id_down}")
                    data_down = db.select_resources(id_down, counter)
                    # Clean data for display
                    data_down = clean_data_for_display(data_down)
                    logger.debug(f"data_down={data_down}")

                    # display code
                    ax_down = fig_down.add_subplot(111)
                    ax_down.set_xlabel("time (ms)")
                    ax_down.set_ylabel(counter)
                    ax_down.plot(*zip(*data_down))
                    fig_canvas_agg_down = draw_figure(window['DOWN CANVAS'].TKCanvas, fig_down)
            else:
                window['ERROR'].update("Please select a counter.")

    window.close()


if __name__ == '__main__':
    main()
