import logging
import os
from matplotlib.figure import Figure

from faasload.utils import try_read_configuration
from faasload.loader import DEFAULTS as DEFAULTS_LOADER
from faasload.docker_monitor import DEFAULTS as DEFAULTS_MONITOR
from faasload.loader.database import FaaSLoadDatabase
from gui import create_dict

logger = logging.getLogger('const')

fig_size = (4, 3)
dpi = 100

fig_up = Figure(figsize=fig_size, dpi=dpi)
fig_down = Figure(figsize=fig_size, dpi=dpi)

conf_loader = try_read_configuration(os.path.expanduser('~/.config/faasload/loader.yml'), DEFAULTS_LOADER)
conf_monitor = try_read_configuration(os.path.expanduser('~/.config/faasload/monitor.yml'), DEFAULTS_MONITOR)

db_cfg = conf_loader['database']
logger.debug('database configuration: %s', db_cfg)

db = FaaSLoadDatabase(db_cfg)

# Fetching data from database
sql_res = db.select_data_for_gui()
res_dict = create_dict(sql_res)
functions_to_display = list(res_dict.keys())

perf_counters = conf_monitor['monitor'].perf_counters
