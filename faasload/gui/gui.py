from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import datetime

from typing import List, Tuple


def draw_figure(canvas, figure) -> FigureCanvasTkAgg:
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg


def delete_fig_agg(fig_agg) -> None:
    fig_agg.get_tk_widget().forget()


def create_dict(lst: list) -> dict:
    """
    Creates a Python dictionary from the SQL query results whose keys are "<function_name>, <start_ms>" and values are
    the resource id.

    :param lst: SQL query result list

    :returns: the corresponding dictionary.
    """
    res = {}
    for tup in lst:
        clef = tup[1] + datetime.datetime.fromtimestamp(int(tup[2]) / 1000).isoformat()
        res[clef] = tup[0]
    return res


def clean_data_for_display(lst: List[Tuple[int, int]]) -> List[Tuple[int, int]]:
    """
    Cleans the SQL query results for display. Tuples are (timestamp, value).

    :param lst: SQL query result list
    :return: the cleaned list
    """
    lst = filter(lambda x: x[0], lst)
    lst = list(map(lambda x: (x[0], y) if (y := x[1]) else (x[0], 0), lst))
    return lst
